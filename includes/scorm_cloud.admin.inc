<?php
// $Id$

/**
 * @file
 * Administration pages for NCFR Paywall.
 */

/**
 * Admin settings form.
 */
function scorm_cloud_settings(&$form_state) {
  $form = array();

  $form['scorm_cloud_info'] = array(
    '#type' => 'fieldset', 
    '#title' => t('SCORM Cloud Info'),
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
    '#description' => t(''),
  );
  $form['scorm_cloud_info']['scorm_cloud_link'] = array(
    '#type' => 'item',
    '#title' => t('Main Scorm Cloud Site'),
    '#value' => l('http://scorm.com', 'http://scorm.com'),
    '#description' => t('Learn more about SCORM Cloud here.'),
  );
  $form['scorm_cloud_info']['scorm_cloud_app_link'] = array(
    '#type' => 'item',
    '#title' => t('Scorm Cloud Application Site'),
    '#value' => l('http://cloud.scorm.com', 'http://cloud.scorm.com'),
    '#description' => t('Create an account and App ID here.'),
  );

  $form['scorm_cloud_config'] = array(
    '#type' => 'fieldset', 
    '#title' => t('SCORM Cloud Configuration Parameters'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
    '#description' => t(''),
  );

  $form['scorm_cloud_config']['scorm_cloud_servicesurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Services URL'),
    '#description' => t('The SCORM Cloud web services URL. Defaults to !url', array('!url' => 'http://cloud.scorm.com/EngineWebServices')),
    '#default_value' => variable_get('scorm_cloud_servicesurl', 'http://cloud.scorm.com/EngineWebServices'),
  );

  $form['scorm_cloud_config']['scorm_cloud_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('AppID'),
    '#default_value' => variable_get('scorm_cloud_appid', ''),
    '#description' => t('You can generate an App ID at !url.', array('!url' => l('http://cloud.scorm.com/', 'http://cloud.scorm.com/'))),
  );

  $form['scorm_cloud_config']['scorm_cloud_secretkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#description' => t('The SCORM Cloud Secret Key. This can be retrieved from !url.', array('!url' => l('http://cloud.scorm.com/', 'http://cloud.scorm.com/'))),
    '#default_value' => variable_get('scorm_cloud_secretkey', ''),
  );

  $form['scorm_cloud_registration'] = array(
    '#type' => 'fieldset', 
    '#title' => t('SCORM Cloud Course Registration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );

  $form['scorm_cloud_registration']['scorm_cloud_registration_email'] = array(
    '#type' => 'select',
    '#title' => t('User Registration Email'),
    '#default_value' => variable_get('scorm_cloud_registration_email', 'mail'),
    '#options' => array(
      'mail' => t('Account Email Address'),
      // @TODO: Implement the 'ask first time functionality'.
      // 'ask_once' => t('Ask first time'),
      'ask' => t('Ask every time'),
    ),
    '#description' => t('The value to use for an aunthenticated user\'s email when registering for a course.'),
  );

  $form['scorm_cloud_registration']['scorm_cloud_registration_firstname'] = array(
    '#type' => 'select',
    '#title' => t('User Registration First Name'),
    '#default_value' => variable_get('scorm_cloud_registration_firstname', 'ask'),
    '#options' => array(
      'ask' => t('Ask every time'),
      'name' => t('Drupal username'),
    ),
    '#description' => t('The value to use for an aunthenticated user\'s first name when registering for a course.'),
  );

  $form['scorm_cloud_registration']['scorm_cloud_registration_lastname'] = array(
    '#type' => 'select',
    '#title' => t('User Registration Last Name'),
    '#default_value' => variable_get('scorm_cloud_registration_lastname', 'ask'),
    '#options' => array(
      'ask' => t('Ask every time'),
      'name' => t('Drupal username'),
    ),
    '#description' => t('The value to use for an aunthenticated user\'s last name when registering for a course.'),
  );

  return system_settings_form($form);
}

/**
 * Confirm the deletion of a course from SCORM Cloud.
 */
function scorm_cloud_admin_course_delete_confirm($form, $course_id) {
  $form = array();
  $form['#course_id'] = $course_id;
  return confirm_form(
    $form, 
    t('Are you sure you want to delete the course with ID %id from SCORM Cloud?', array('%id' => $course_id)), 
    'admin/settings/scorm_cloud/courses', 
    t('This action cannot be undone.'), 
    t('Delete'), 
    t('Cancel'), 
    'scorm_cloud_admin_course_delete_confirm');
}

/**
 * Confirm the deletion of a course from SCORM Cloud.
 */
function scorm_cloud_admin_course_delete_confirm_submit($form, &$form_state) {
  scorm_cloud_delete_course($form['#course_id']);
  drupal_set_message(t('The course and all of it\'s registrations have been deleted.'));
  $form_state['redirect'] = 'admin/settings/scorm_cloud/courses';
}



/**
 * Confirm the deletion of a course from SCORM Cloud.
 */
function scorm_cloud_admin_registration_delete_confirm($form, $reg_id) {
  $form = array();
  $form['#registration_id'] = $reg_id;
  return confirm_form(
    $form, 
    t('Are you sure you want to delete the registration with ID %id?', array('%id' => $reg_id)),
    'admin/settings/scorm_cloud/registrations', 
    t('This action cannot be undone.'), 
    t('Delete'), 
    t('Cancel'), 
    'scorm_cloud_admin_course_delete_confirm');
}

/**
 * Confirm the deletion of a course from SCORM Cloud.
 */
function scorm_cloud_admin_registration_delete_confirm_submit($form, &$form_state) {
  scorm_cloud_delete_registration($form['#registration_id']);
  drupal_set_message(t('The registration has been deleted.'));
  $form_state['redirect'] = 'admin/settings/scorm_cloud/registrations';
}
/**
 * Show the admin defined reports.
 */
function scorm_cloud_reports($report = NULL) {
  // Make sure there's an app id
  if ($app_id = variable_get('scorm_cloud_appid', '')) {
    $auth = scorm_cloud_api_call('Reporting', 'getReportageAuth', array('DOWNONLY'));
  
    if (class_exists('WidgetSettings')) {
      $settings = new WidgetSettings();
      $settings->setIframe(false);
      $settings->setStandAlone(true);
      $settings->setEmbedded(true);
      $settings->setScriptBased(true);
      $settings->setVertical(true);
      $settings->setDivName('scormsummary');
      
      $url = scorm_cloud_api_call('Reporting', 'GetWidgetUrl', array($auth, 'allSummary', $settings));
    }
  
    drupal_set_html_head('<script type="text/javascript" src="http://cloud.scorm.com/Reportage/scripts/reportage.combined.js"></script>');
    drupal_set_html_head('<link rel="stylesheet" type="text/css" href="http://cloud.scorm.com/Reportage/css/reportage.combined.css"></link>');
    drupal_add_js('
      $(document).ready( function() {	
        loadScript("'. $url .'");
      });
    ', 'inline');

    // Add a link to the reportage.
    $auth = scorm_cloud_api_call('Reporting', 'getReportageAuth', array('FREENAV'));
    $url = scorm_cloud_api_call('Reporting', 'GetReportUrl', array($auth, 'http://cloud.scorm.com/Reportage/reportage.php?appId='. $app_id));
    $link = l('Access more SCORM Cloud reports here', $url);;
  }
  return '<div id="scormsummary">Loading report.</div>' . $link;
}

/**
 * Service testing form.
 */
function scorm_cloud_apitest_debug() {
  include_once './includes/install.inc';
  include_once './'. drupal_get_path('module', 'scorm_cloud') . '/includes/scorm_cloud.api.inc';
  
  $ping                 = scorm_cloud_api_call('Debug', 'CloudPing');
  $pingauth             = scorm_cloud_api_call('Debug', 'CloudAuthPing');

  $apicoursescount      = scorm_cloud_count_courses_api();
  $apiregscount         = scorm_cloud_count_registrations_api();

  $drupalcoursescount   = scorm_cloud_count_courses_drupal();
  $drupalregscount      = scorm_cloud_count_registrations_drupal();

  $status = array(
    array(
      'title' => 'CloudPing',
      'value' => $ping ? t('OK') : t('Failed'),
      'severity' => $ping ? REQUIREMENT_OK : REQUIREMENT_ERROR,
      
    ),
    array(
      'title' => 'CloudAuthPing',
      'value' => $pingauth ? t('OK') : t('Failed'),
      'severity' => $pingauth ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    ),
    array(
      'title' => 'Courses (in Drupal/in Scorm CLOUD)',
      'value' => l($drupalcoursescount . '/' . $apicoursescount, 'admin/settings/scorm_cloud/courses'),
      'severity' =>  $drupalcoursescount == $apicoursescount ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    ),
    array(
      'title' => 'Registrations (in Drupal/in Scorm CLOUD)',
      'value' => l($drupalregscount . '/' . $apiregscount, 'admin/settings/scorm_cloud/registrations'),
      'severity' =>  $drupalregscount == $apiregscount ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    ),
  );

  return theme('status_report', $status);
}

/**
 * Run CheckCredentials.
 */
function scorm_cloud_apitest_debug_submit($form, &$form_state) {
  include_once './'. drupal_get_path('module', 'scorm_cloud') . '/includes/scorm_cloud.api.inc';
  //dpm($form_state);
  return(scorm_cloud_ping($form_state['values']['op']));
}

/**
 * Get a count of all of the courses available through the API.
 */
function scorm_cloud_count_courses_api() {
  $courses = scorm_cloud_admin_get_courses_api();
  return count($courses);
}

/**
 * Get a count of all of the courses available through the API.
 */
function scorm_cloud_count_courses_drupal() {
  $courses = scorm_cloud_admin_get_courses_drupal();
  return count($courses);
}


/**
 * Get a count of all of the courses available through the API.
 */
function scorm_cloud_count_registrations_api() {
  $regs = scorm_cloud_admin_get_registrations_api();
  return count($regs);
}

/**
 * Get a count of all of the courses available through the API.
 */
function scorm_cloud_count_registrations_drupal() {
  return scorm_cloud_get_user_registrations_count();
}


/**
 * Course listing page.
 */
function scorm_cloud_admin_courses_list() {
  $rows = array();
  $headers = array();

  $api_courses = scorm_cloud_admin_get_courses_api();
  $drupal_courses = scorm_cloud_admin_get_courses_drupal();
 
  $course_fields = array(
    '_courseId' => t('ID'),
    '_title' => t('Title'),
    '_numberOfVersions' => t('Versions'),
    '_numberOfRegistrations' => t('Registrations'),
  );
  $headers = array(
    t('Course ID'),
    t('Title'),
    t('Versions'),
    t('Registration'),
    t('In Scorm CLOUD'),
    t('In Drupal'),
  );

  // List the courses in the SCORM Cloud API
  foreach ($api_courses as $course_obj) {
    $course_id = $course_obj->getCourseId();

    $course = array();
    $course['id'] = $course_obj->getCourseId();
    $course['title'] = $course_obj->getTitle();
    $course['versions'] = $course_obj->getNumberOfVersions();
    $course['registrations'] = $course_obj->getNumberOfRegistrations();
    $course['in_scorm'] = array('data' => t('Yes'), 'class' => 'error');
    $course['in_drupal'] = array('data' => t('No'), 'class' => 'error');

    if (isset($drupal_courses[$course_id])) {
      $course['in_drupal'] = array('data' => t('Yes') . ' ('. l(t('view'), 'node/'. $drupal_courses[$course_id]['nid']) . ')');
      $course['in_scorm']['class'] = $course['in_drupal']['class'] = 'ok';
      unset($drupal_courses[$course_id]);
    }
    else {
      // Add delete link.
      $course['in_scorm']['data'] .= ' ('. l(t('delete'), 'admin/settings/scorm_cloud/courses/delete/'. $course_obj->getCourseId()) . ')';
    }

    $rows[] = $course;
  }

  // List the courses in drupal but not in SCORM.
  foreach ($drupal_courses as $course_id => $course_array) {
    $course = array();
    $course['id'] = $course_id;
    $course['title'] = empty($course_array['title']) ? '-' : $course_array['title'];
    $course['versions'] = '-';
    $course['registrations'] = '-';
    $course['in_scorm'] = array('data' => t('No'), 'class' => 'error');

    foreach ($course_array['nids'] as $nid) {
      $course['in_drupal'] = array('data' => t('Yes') . ' ('. l(t('view'), 'node/'. $nid) . ' | '. l(t('delete'), 'node/'. $nid . '/delete', array('query' => drupal_get_destination())) . ')', 'class' => 'error');
      $rows[] = $course;
    }
  }

  return theme('table', $headers, $rows);
}



/**
 * Course listing from the API.
 */
function scorm_cloud_admin_get_courses_api() {
  static $courses = NULL;

  if ($courses === NULL) {
    $courses = scorm_cloud_api_call('Course', 'getCourseList');
  }
  return $courses;
}

/**
 * Course listing from the API.
 */
function scorm_cloud_admin_get_courses_drupal() {
  return scorm_cloud_get_course_list();
}

/**
 * Registration listing from the API.
 */
function scorm_cloud_admin_get_registrations_api($course_id = '') {
  static $registrations = array();

  if (!isset($registrations[$course_id])) {
    $params = array();
    if ($course_id) {
      $params['coursefilter'] = $course_id;
    }
    $items = scorm_cloud_api_call_method('rustici.registration.getRegistrationList', $params);
    $registrations[$course_id] = isset($items->registrationlist->registration) ? $items->registrationlist->registration : array();
  }
  return $registrations[$course_id];
}

/**
 * Registration listing from the API.
 */
function scorm_cloud_admin_get_registrations_drupal($course_id = '') {
  static $registrations = array();

  if ($course_id) {
    return scorm_cloud_get_course_registrations($course_id);
  }
  else {
    return scorm_cloud_get_registrations();
  }
}
/**
 * Course upload form.
 */
function scorm_cloud_admin_course_import() {
  $form = array();
  $import_url = url('admin/settings/scorm_cloud/courses/upload/finish', array('absolute' => TRUE));
  $upload_url = scorm_cloud_api_call('Upload', 'GetUploadUrl', array($import_url));

  $form['#action'] = $upload_url;
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $form['filedata'] = array(
    '#title' => t('Upload a SCORM File'),
    '#type' => 'file',
    '#description' => t("Upload SCORM course here to be referenced by Id with SCORM Course Id CCK field -OR- upload SCORM course more conveniently on course node with SCORM Course CCK field. Max file size: %size", array("%size" => format_size(file_upload_max_size()))),
  );

  $form['scorm_cloud_listfiles'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
  );

  return $form;
}

/**
 * Course registration callback.
 */
function scorm_cloud_admin_course_import_finish() {
  if ($location = $_GET['location']) {
    $courseId = uniqid();
    scorm_cloud_api_call('Course', 'ImportUploadedCourse', array($courseId,$location));
  }
  drupal_goto('admin/settings/scorm_cloud/courses/list');
}

/**
 * Course listing page.
 */
function scorm_cloud_admin_registrations_list($course_id = NULL) {
  $rows = array();
  $headers = array(
    'registrationId',
    'courseId',
    'courseTitle',
    'learnerId',
    'learnerFirstName',
    'learnerLastName',
    'email',
    'createDate',
    t('In SCORM Cloud'),
    t('In Drupal'),
  );

  $drupal_registrations = scorm_cloud_admin_get_registrations_drupal($course_id);

  $items = scorm_cloud_admin_get_registrations_api($course_id);
  if (!empty($items)) {
    foreach ($items as $item) {
      //$item = $item->registration;
      $row = array();
      foreach ($headers as $key) {
        $row[$key] = empty($item->{$key}) ? '' : (string)$item->{$key};
      }

      $row[t('In SCORM Cloud')] = array('data' => t('Yes'), 'class' => 'error');
      $row[t('In Drupal')] = array('data' => t('No'), 'class' => 'error');

      if (isset($drupal_registrations[$row['registrationId']])) {
        $row[t('In Drupal')] = array('data' => t('Yes'));
        $row[t('In Drupal')]['class'] = $row[t('In SCORM Cloud')]['class'] = 'ok';
        unset($drupal_registrations[$row['registrationId']]);
      }
      else {
        $row[t('In SCORM Cloud')]['data'] .= ' (' . l(t('delete'), 'admin/settings/scorm_cloud/registrations/delete/' . $row['registrationId']) . ')';
      }

      $rows[] = $row;
    }
  }

  foreach ($drupal_registrations as $id => $reg) { 
    $row = array(
      'registrationId' => $id,
      'course_id' => $course_id,
      'courseTitle' => '-',
      'learnerId' => $reg->uid,
      'learnerFirstName' => '',
      'learnerLastName' => '',
      'email' => '',
      'createDate' => format_date($reg->date_first_launched),
      'in_scorm' => array('data' => t('No'), 'class' => 'error'),
      'in_drupal' => array('data' => t('Yes') . ' (' . l(t('delete'), 'admin/settings/scorm_cloud/registrations/delete/' . $id) . ')', 'class' => 'error'),
    );
    $rows[] = $row;
  }

  return theme('table', $headers, $rows);
}

/**
 * Add a new registration.
 */
function scorm_cloud_admin_registrations_add($form_state) {
  $form = scorm_cloud_register_form($form_state, '');

  unset($form['uid']);
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#description' => t('Enter a Drupal username'),
    '#autocomplete_path' => 'user/autocomplete',
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#required' => TRUE,
    '#weight' => -2,
  );
  $form['course_id'] = array(
    '#title' => t('Course ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -1,
  );
  return $form;
}


/**
 * Add a new registration.
 */
function scorm_cloud_admin_registrations_add_validate($form, $form_state) {
  if (!$account = user_load(array('name' => $form_state['values']['username']))) {
    form_set_error('username', t('Username not found'));
  }
}

/**
 * Add a new registration.
 */
function scorm_cloud_admin_registrations_add_submit($form, $form_state) {
  if ($account = user_load(array('name' => $form_state['values']['username']))) {
    $course_id  = $form_state['values']['course_id'];
    $firstname  = $form_state['values']['firstname'];
    $lastname   = $form_state['values']['lastname'];
    $email      = $form_state['values']['email'];
    $uid        = $account->uid;

    $registration = scorm_cloud_register_user($course_id, $uid, $firstname, $lastname, $email);
  }
}
