<?php

/**
 * @file
 *   drush integration for scorm_cloud.
 */

/**
 * The SCORM Cloud PHP library URI.
 */
define('SCORM_CLOUD_DOWNLOAD_URI', 'https://nodeload.github.com/RusticiSoftware/SCORMCloud_PHPLibrary/tarball/master');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function scorm_cloud_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['scorm-cloud-dl'] = array(
    'callback' => 'drush_scorm_cloud_download_library',
    'description' => dt("Downloads the SCORM Cloud PHP library."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'path' => dt('Optional. A path where to install the SCORM Cloud PHP library. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function scorm_cloud_drush_help($section) {
  switch ($section) {
    case 'drush:scorm-cloud-dl':
      return dt("Downloads the SCORM Cloud PHP library from github, default location is sites/all/libraries.");
  }
}

/**
 * Command to download the SCORM Cloud PHP library.
 */
function drush_scorm_cloud_download_library() {
  if (!drush_shell_exec('type unzip')) {
    return drush_set_error(dt('Missing dependency: unzip. Install it before using this command.'));
  }

  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }
  if (!is_dir($path . '/scorm_cloud')) {
    drush_op('mkdir', $path . '/scorm_cloud');
    drush_log(dt('Directory @path was created', array('@path' => $path . '/scorm_cloud')), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  $filename = basename(SCORM_CLOUD_DOWNLOAD_URI);
  $dirname = $path . '/scorm_cloud';

  // Remove any existing scorm_cloud plugin directory
  if (is_dir($dirname)) {
    drush_log(dt('A existing SCORM Cloud PHP library was overwritten at @path', array('@path' => $path)), 'notice');
  }
  // Remove any existing SCORM Cloud PHP library zip archive
  if (is_file($filename)) {
    drush_op('unlink', $filename);
  }

  // Download the zip archive
  if (!drush_shell_exec('wget ' . SCORM_CLOUD_DOWNLOAD_URI)) {
    drush_shell_exec('curl -O ' . SCORM_CLOUD_DOWNLOAD_URI);
  }

  if (is_file($filename)) {
    // Decompress the zip archive
    drush_shell_exec('tar -xz --strip-path=1 --directory scorm_cloud -f' . $filename);
    drush_op('unlink', $filename);
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

  if (is_dir($path . '/' . $dirname)) {
    drush_log(dt('Drush was unable to download the SCORM Cloud PHP library to @path', array('@path' => $path)), 'error');
  }
  else {
    drush_log(dt('SCORM Cloud PHP library has been downloaded to @path', array('@path' => $path)), 'success');
  }
}
