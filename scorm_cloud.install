<?php

/**
 * Implementation of hook_schema().
 */
function scorm_cloud_schema() {
  $schema['scorm_cloud_registration'] = array(
    'description' => t('SCORM Cloud registrations. Each row refers to a single registration in SCORM Cloud.'),
    'fields' => array(
      'id' => array(
        'description' => t('The unique ID for the registration'),
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => t('The {node}.nid of the node that this registration is connected to (if any).'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'course_id' => array(
        'description' => t('The course id that this registration is for'),
        'type' => 'varchar',
        'length' => '64',
        'not null' => FALSE,
      ),
      'uid' => array(
        'description' => t('The {user}.uid of the registrant.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'date_first_launched' => array(
        'description' => t('The date that the course was first launched. Retrieved via the API.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'date_last_launched' => array(
        'description' => t('The date that the course was last launched. Retrieved via the API.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'date_completed' => array(
        'description' => t('The date that the course was completed. Retrieved via the API.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'complete' => array(
        'description' => t('Whether or not the course was completed. Retrieved via the API.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'success' => array(
        'description' => t('Whether or not the course was passed. Retrieved via the API.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'score' => array(
        'description' => t('The score earned. Retrieved via the API.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'totaltime' => array(
        'description' => t('Time spent taking the course. Retrieved via the API.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'num_launches' => array(
        'description' => t('The number of times that the course was launched. Retrieved via the API.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'take_number' => array(
        'description' => t('The sequence number of the registration if multiple exist for a user and course.'),
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'course_id' => array('course_id'),
      'uid' => array('uid'),
    ),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function scorm_cloud_install() {
  drupal_install_schema('scorm_cloud');
}

/**
 * Remove variables on uninstall.
 */
function scorm_cloud_uninstall() {
  drupal_uninstall_schema('scorm_cloud');

  drupal_load('module', 'content');
  content_notify('uninstall', 'scorm_cloud');

  db_query("DELETE FROM {variable} WHERE name LIKE 'scorm_cloud_%'");
  cache_clear_all('variables', 'cache');
}
